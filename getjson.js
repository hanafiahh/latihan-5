const express = require('express');
const app = express();

app.get('/', (req, res) => {
    res.send('Salam Muafakat!');
});

app.get('/johor', (req, res) => {
    res.json({ data: "Salam Muafakat" });
});

app.get('/uitm', (req, res) => {
    res.json({ data: "Yahooo buat API berjaya" });
});

app.listen(8080, () => {
    console.log('App listening on port 8080!');
});
